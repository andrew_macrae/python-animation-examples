#!/usr/bin/env python
"""
This example shows the spatial profile of a Michelson interferometer as one arm is scanned.
This is used as an example for the University of Victoria Physics 325 (Optics) course.
Created by Andrew MacRae
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

fig = plt.figure()

sz = 111        # size of grid (sz X sz)
L = 1           # Distance from source, to end of interfeometer (relevant for divergence of beam)
lmd = 6e-5      # wavelength of light
dl = 200*lmd    # Initial relative path distance
dR = .1         # Size of observation screen (2dR x 2dR)

def f(x, y, z):
    '''
    The geometric beam difference at a point on the screen due to diverging input beam
    All the physics is here, the rest is plotting
    
    '''
    r = np.sqrt(x**2+y**2)                              # Height on screen (think Pythagoras)
    dx = np.sqrt((L+dl + z)**2+r**2)-np.sqrt(L**2+r**2) # The difference of hypotenuses <- hypotenae?
    return np.cos(2*np.pi*dx/lmd)**2                    # The optical phase due to this difference

# Create an x-y grid
x = np.linspace(-dR, dR, sz)
y = np.linspace(-dR, dR, sz).reshape(-1, 1) # need to reshape to form an 'outer product'
z = 0 # The parameter we wish to scan

# This creates the plot ...
im = plt.imshow(f(x, y, z), cmap=plt.get_cmap('afmhot'), animated=True) 

# ... this updates it. We access global variable z, increment it, then re-plot
def updatefig(*args):
    global z
    z += lmd/25
    
    im.set_array(f(x, y, z))
    return im,

# Create the animation
ani = animation.FuncAnimation(fig, updatefig, interval=50, blit=False)
plt.show()