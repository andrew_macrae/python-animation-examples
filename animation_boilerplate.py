import numpy as np
from matplotlib import pyplot as plt
from matplotlib import animation

# ------------------------------------------------------------------------------------------------------------------------
# example 1a: Minimal Example: single line, single plot
# Super Basic example: Propagating Gaussian pulse. No frills, no gimmicks
# There are three stages to animation using this method - I:initialization, II: definition, III: and creation of plot 
# ------------------------------------------------------------------------------------------------------------------------

# ------====== I: Initialization ======-------
# Animation requires an initialization function as an argument.
def init():
    line.set_data([], [])
    return line

# It's useful to define several globals here and specify plot range/parameters in terms of these:
nFrames = 50
zMin = -15
zMax = 15
z = np.linspace(zMin,zMax,2**14)
tMin = -6
tMax = 6

# Finally, set up the canvas: Note that it seems that you must set the axes up or you don't get a display ...
fig = plt.figure()
ax = plt.axes(xlim=(tMin, tMax), ylim=(0, 1))
line, = ax.plot([], [], lw=2)

# ------====== II: Plot definition ======-------
# animate takes a single argument t: an integer which can be treated as an iterator that can be mapped to variable
def animate(t):    
    # convert t to the parameter to be animated
    z0 = (t-nFrames/2)*zMax/nFrames
# Next make the plot vector
    s0 = .5
    pulse = np.exp(-0.5*((z-z0)/s0)**2)
# Write the data to the list    
    line.set_data(z, pulse)

# ---- III: Plot creation ----
# first create an animation object using the figure handle, init function, frames set previously
# interval draws a new frame every interval milliseconds (below is thus 2.5s) blit must be False due to an OSX bug
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=50, interval=50, blit=False)
# Finally, we can save to file
fName = '/Users/amacrae/Desktop/test1.gif'
anim.save(fName, writer='imagemagick')

# ------------------------------------------------------------------------------------------------------------------------
# example 1b: single line, single plot. Now with some  frills and gimmicks
# Here, the example is similar, but now the pulse propagates through an atomic medium
# We also add plot axis labels, titles, and text which display the information of the current frame
# ------------------------------------------------------------------------------------------------------------------------

# ------====== I: Initialization ======-------
def init():
    line.set_data([], [])
    return line
# Plot globals 
nFrames = 50

#Solve propagation in the frequency domain under steady state atomic response and dynamic optical response
Gamma = 1;
spn = 100;
dMax = 4
kz = 1
# frequency span in units of natural linewidth
w = np.linspace(-100*Gamma,100*Gamma,2**12)
# corresponding time vector
tm = w/(Gamma*spn);

# Finally, set up the canvas
# this time we'll add axis labels, a title and text displying the parameter of interest
fig = plt.figure()
tmp = 1
ax = plt.axes(xlim=(np.min(tm)/tmp, np.max(tm)/tmp), ylim=(0, 1))
line, = ax.plot([], [], lw=2)
time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
time_template = 'Detuning: %.1f$\Gamma$'

plt.xlabel('z-vt')
plt.ylabel('Transmission')
plt.title('Pulse Propagation of a 2-level atom for varying detuning')

#steady state susceptibility
# quick thought, can we partially account for non-steady state atomic response by weighing in freq domain? Maybe!
def chi2lev(Ws,G):
    return 1/(Ws-1j*G/2)

# ------====== II: Plot definition ======-------
# animate takes a single argument t: an integer which can be treated as an iterator that can be mapped to variable
def animate(t):    
# convert t to the parameter to be animated
    w0 = -(nFrames/2-t)*dMax/nFrames
# Next make the plot vector
    
    dW = Gamma/5;    # bandwidth of optical pulse

    E0 = np.exp(-.5*((w-w0)/dW)**2)
    Ef = E0*np.exp(1j*kz*chi2lev(w,Gamma))
    
    Et0 = np.abs(np.fft.fftshift(np.fft.ifft(E0)));
    scl = np.max(Et0);
    Etf = np.abs(np.fft.fftshift(np.fft.ifft(Ef)))/scl

# Write the data to the list  
    time_text.set_text(time_template % (w0))
    line.set_data(tm, Etf)
    return line, time_text
# ---- III: Plot creation ----
# first create an animation object using the figure handle, init function, frames set previously
# interval draws a new frame every interval milliseconds (below is thus 2.5s) blit must be False due to an OSX bug
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=50, interval=100, blit=False)
# Finally, we can save to file
fName = '/Users/amacrae/Desktop/test2.gif'
anim.save(fName, writer='imagemagick')    

# ------------------------------------------------------------------------------------------------------------------------
# example 2: multiple lines on one plot
# Demonstration of "fast light" i.e. group velocity > c in a 2-level atom
# Here the plot contains three lines and a legend.
# ------------------------------------------------------------------------------------------------------------------------

# ------====== I: Initialization ======-------
# Plot globals 
nFrames = 50

#Solve propagation in the frequency domain under steady state atomic response and dynamic optical response
Gamma = 1;
spn = 100;
dMax = 4
w0 = 0.9
dW = Gamma/5
# frequency span in units of natural linewidth
w = np.linspace(-100*Gamma,100*Gamma,2**12)
# corresponding time vector
tm = w/(Gamma*spn);

# Finally, set up the canvas
# this time we'll add axis labels, a title and text displying the parameter of interest
fig = plt.figure()
tmp = 2
ax = plt.axes(xlim=(np.min(tm)/tmp, np.max(tm)/tmp), ylim=(0, 1))
ax.grid(True)
# First difference is here: Create three lines
lines = []
markers, linestyles = ['None','None','None'], ['-','-','--']
plotlays, plotcols = [len(markers)], ["gray","red","lightcoral"]
for index in range(len(markers)):
    lobj = ax.plot([],[],lw=2,color=plotcols[index],marker=markers[index],linestyle=linestyles[index])[0]
    lines.append(lobj)

# and initialize each of them
def init():
    for line in lines:
        line.set_data([], [])
    return lines

plt.xlabel('z-vt')
plt.ylabel('Transmission')
plt.title('\"Superluminal\" group velocity in a 2-level atom')
plt.legend(['Free Space','2-level Atom','Normailized'],loc=2)
def chi2lev(Ws,G):
    return 1/(Ws-1j*G/2)

def animate(t):
    xlist = [tm,tm,tm]
    
    kz = t/8
    E0 = np.exp(-.5*((w-w0)/dW)**2)
    Ef = E0*np.exp(1j*kz*chi2lev(w,Gamma))
    #Ef = E0*np.exp(1j*kz*chi3lev(w,0,Gamma,0,1))
    
    Et0 = np.abs(np.fft.fftshift(np.fft.ifft(E0)));
    scl = np.max(Et0);
    Et0 = Et0/scl
    Etf = np.abs(np.fft.fftshift(np.fft.ifft(Ef)))/scl
    scl = np.max(Etf);
    EtfN = Etf/scl
    
    ylist = [Et0,Etf,EtfN]
    for lnum,line in enumerate(lines):
        line.set_data(xlist[lnum], ylist[lnum]) # set data for each line separately. 
# ---- III: Plot creation ----
# first create an animation object using the figure handle, init function, frames set previously
# interval draws a new frame every interval milliseconds (below is thus 2.5s) blit must be False due to an OSX bug
anim = animation.FuncAnimation(fig, animate, init_func=init, frames=50, interval=100, blit=False)
# Finally, we can save to file
fName = '/Users/amacrae/Desktop/test3.gif'
anim.save(fName, writer='imagemagick')    